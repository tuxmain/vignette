    //---- Error and message strings for JS code

    const STRING_ERROR_TITLE_PUBKEY_NOT_PROVIDED  = "ERROR : No s'ha especificat el títol o la clau pública!";
    const Function_STRING_WARNING_SAME_PUBKEY     = (creator_pubkey) => `AL TANTO! Heu introduit la clau pública de viĞnette: ${creator_pubkey}. És això el que volieu fer?`
    const Function_STRING_ERROR_INVALID_PUBKEY    = (pubkey) => `La clau pública ${pubkey} no està validada`;
    const Function_STRING_ERROR_WRONG_CHECKSUM = (checksum, pubkey) => `El checksum ${checksum} no correspon a la clau pública ${pubkey}. Si us plau, comprobeu la clau pública.`
    const STRING_ERROR_BROWSER_NOT_SUPPORTED      = "ERROR : El vostre navegador no está suportat."
    const Function_STRING_ERROR_PUBKEY_TOO_SHORT  = (min_length) => `ERROR : la clau pública fa menys de ${min_length} caràcters.\n`
    const Function_STRING_ERROR_PUBKEY_TOO_LONG   = (max_length) => `ERROR : la clau pública fa més de ${max_length} caràcters.\n`
    const Function_STRING_SEND_DONATION               = (pubkey_ck) => `Si voleu fer una donació a Viğnette, la podeu fer a la clau pública: ${pubkey_ck}.`
    const STRING_PUBKEY_COPIED_IN_CLIPBOARD       = "\n\nAquesta clau pública ha sigut copiada al vostre portapapers. Ara la podeu enganxar-directament a Cesium."