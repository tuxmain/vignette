    //---- main script for image generation ----
    const CREATOR_PUBKEY = "TaJGgFayeGBouUNJcUPwvGTQWxh9iz4ghDGD887ZTSe";
    const CREATOR_PUBKEY_CK = "TaJGgFayeGBouUNJcUPwvGTQWxh9iz4ghDGD887ZTSe:EsZ";

    const MAX_PUBKEY_LENGTH = 44;
    const MIN_PUBKEY_LENGTH = 43;

    //---- canvas dimensions ----
    const CANVAS_HEIGHT = 2362; // 20cm @300dpi
    const CANVAS_WIDTH = 3307; // 28cm @300dpi
    const MARGIN_CANVAS = 150;
    const FRAME_LINE_SIZE = 1;

    const QR_WIDTH_AND_HEIGHT = 600;
    const IMAGE_WIDTH_AND_HEIGHT = 900;
    const LOGO_WIDTH_AND_HEIGHT = 525;

    const TITLE_FONT = "135px -apple-system,'BlinkMacSystemFont','Segoe UI','Roboto','Oxygen-Sans','Ubuntu','Cantarell','Helvetica Neue','sans-serif'";

    const INFOS_FONT = "90px -apple-system,'BlinkMacSystemFont','Segoe UI','Roboto','Oxygen-Sans','Ubuntu','Cantarell','Helvetica Neue','sans-serif'";
    const INFOS_START_POSITION = 350;
    const INFOS_LINE_BREAK = 150;

    const CKSUM_COLOR = "#686868"

    const PUBKEY_URI_FONT = "60px -apple-system,'BlinkMacSystemFont','Segoe UI','Roboto','Oxygen-Sans','Ubuntu','Cantarell','Helvetica Neue','sans-serif'";

    const G1_WRONG_SYMBOL = "G1";
    const G1_SYMBOL = "Ğ1";
    const G_WARN_SYMBOL = "_G_";
    const G_SYMBOL = "Ğ";
    const G1_MIN_WRONG_SYMBOL = "g1";
    const G1_MIN_SYMBOL = "ğ1";
    const G_MIN_WARN_SYMBOL = "_g_";
    const G_MIN_SYMBOL = "ğ";

    //----------- main script -----------------

    async function createVignette(event) {
      event.preventDefault();
      // get elements
      let title = document.forms["formulaire"]["title"].value;
      let infos = document.forms["formulaire"]["infos"].value;
      let pubkey = document.forms["formulaire"]["pubkey"].value;
      let image = document.forms["formulaire"]["image"].files[0];
      let isLogo = document.forms["formulaire"]["logo"].checked;  
      let isCard = document.forms["formulaire"]["biz_card"].checked;
      let numberOfVignettes = document.forms["formulaire"]["card_number"].value;
      // abort if elements are not there
      if (! checkValues(title, infos, pubkey, image)) {
        return;
      }
      // check pubkey + ck and add checksum if necessary
      // alert if pubkey+ck is wrong
      pubkey = await checkPubKeyAndChecksum(pubkey);
      if (pubkey == 0) {
        return;
      }
      // reset QRcode
      document.getElementById("vignetteQrCode").remove()
      // create QRcode of Pubkey
      let qrCode = document.createElement("div");
      qrCode.setAttribute("id", "vignetteQrCode");
      qrCode.setAttribute("style", "display: none;"); //hide PNG image
      new QRCode(qrCode, {
        text: pubkey,
        width: QR_WIDTH_AND_HEIGHT,
        height: QR_WIDTH_AND_HEIGHT,
      });
      document.getElementById("imageCanvas").appendChild(qrCode)
      // get user image and launch Vignette creation
      if (image) {
        let reader = new FileReader();
        reader.onload = function () {
          let tempImage = document.getElementById("temp_image")
          tempImage.onload = function () {
            vignetteCanvas = createPrintableVignette( qrCode, title, pubkey, infos, tempImage, isLogo, isCard, numberOfVignettes);
          }
          tempImage.src = reader.result
        }
      reader.readAsDataURL(image);
      // if no image ; use default logo
      } else {
        isLogo = false; // No user image : don't display the logo twice
        let tempImage = document.getElementById("temp_image")
        tempImage.src = IMAGE_PLACEHOLDER;  
        tempImage.onload = function () {
          vignetteCanvas = createPrintableVignette( qrCode, title, pubkey, infos, tempImage, isLogo, isCard, numberOfVignettes);
        }
      }

      // make image canvas appear as PNG
      let imageCanvas = document.getElementById("imageCanvas");
      imageCanvas.setAttribute("style", "display: block; width: 100%; height: auto;");

      // add event listeners on buttons
      let printButton = document.getElementById("print");
      printButton.number = numberOfVignettes;
      handleEventListener(printButton, "click", printVignetteOnClick)
      function printVignetteOnClick (event) {
        printVignette(event.currentTarget.number);
      }
      let copyPubkeyButton = document.getElementById("copy");
      handleEventListener( copyPubkeyButton, "click", copyPubkey)
      let downloadButton = document.getElementById("download")
      handleEventListener( downloadButton, "click", downloadVignette );
    }

    function handleEventListener(element, event, func) {
      if ( ! element.classList.contains(event)) {
        element.classList.add(event);
        element.addEventListener(event, func);
      }
    }

    function checkValues (title, infos, pubkey, image) {
      // vérifier qu'on a au moins titre, infos et pubkey
      if (! title || ! pubkey ) {
        alert(STRING_ERROR_TITLE_PUBKEY_NOT_PROVIDED);
        return false;
      } else 
      if ((pubkey == CREATOR_PUBKEY) || (pubkey == CREATOR_PUBKEY_CK)) {
        if (! confirm (Function_STRING_WARNING_SAME_PUBKEY(CREATOR_PUBKEY))){
          return false;
        }
      }
      return true;
    }

    async function checkPubKeyAndChecksum(pubkey) {
      // separate pubkey from checksum and test formats.
      let separatePubkey;
      let checksum;
      separatePubkey = pubkey.trim().split(":");
      if (! verifyFormat(separatePubkey)){
        return 0
      };
      // compute pubkey checksum
      //checksum = public_key_checksum_from_b58(separatePubkey[0]);
      checksum = await public_key_checksum_from_b58(separatePubkey[0])
      console.log(checksum)
      if (checksum === -1) {
        alert ( Function_STRING_ERROR_INVALID_PUBKEY(separatePubkey[0]) );
        return 0;
      }
      // verify checksum if one
      if (separatePubkey[1]) {
        pubkey = verifyChecksum(separatePubkey, checksum);          
      } else {
        pubkey = (separatePubkey[0] + ":" + checksum);
      }
      return pubkey;
    }

    function verifyChecksum (pubkeyAndChecksum, checksum) {
      // gets a list with pubkey and checksum, compares with good checksum.
      if ( pubkeyAndChecksum[1] !== checksum) {
        alert ( Function_STRING_ERROR_WRONG_CHECKSUM(pubkeyAndChecksum[1], pubkeyAndChecksum[0]) );
        return false;
      }
      return (pubkeyAndChecksum[0] + ":" + checksum)
    }

    function verifyFormat (pubkeyAndChecksum) {
      // check format
      let errorMessage = "";
      if ( pubkeyAndChecksum[0].length < MIN_PUBKEY_LENGTH ) {
        errorMessage = (Function_STRING_ERROR_PUBKEY_TOO_SHORT(MIN_PUBKEY_LENGTH))
      } else if ( pubkeyAndChecksum[0].length > MAX_PUBKEY_LENGTH ) {
        errorMessage = (Function_STRING_ERROR_PUBKEY_TOO_LONG(MAX_PUBKEY_LENGTH))
      }
      if (errorMessage !== ""){
        alert( errorMessage );
       return false;
      }
      return true;
    }

    function createPrintableVignette (qrCode, title, pubkey, infos, givenImage, isLogo, isCard, vignetteNumber) {

      let frameLineSize = 0;
      if (vignetteNumber != 1) {
        frameLineSize = Math.floor(FRAME_LINE_SIZE * Math.sqrt(vignetteNumber));
      }
      let fullSizeVignette = createVignetteCanvas (qrCode, title, pubkey, infos, givenImage, isLogo, isCard, frameLineSize)

      // see https://developer.mozilla.org/fr/docs/Web/API/CanvasRenderingContext2D/drawImage
      let canvas = document.getElementById("vignette_canvas");
      if (canvas.getContext) {
        let ctx = canvas.getContext('2d');
        canvas.setAttribute ("width", CANVAS_WIDTH);
        canvas.setAttribute ("height", CANVAS_HEIGHT);
        switch (vignetteNumber) {
          case '1':
            ctx.drawImage(fullSizeVignette, 0, 0);
            break;
          case '2':
            // portrait orientation for page
            canvas.setAttribute ("width", CANVAS_HEIGHT);
            canvas.setAttribute ("height", CANVAS_WIDTH);
            ctx.drawImage(fullSizeVignette, 0, 0, CANVAS_HEIGHT , CANVAS_WIDTH / 2);
            ctx.drawImage(fullSizeVignette, 0 , CANVAS_WIDTH / 2 , CANVAS_HEIGHT , CANVAS_WIDTH / 2);
            break;
          default:
            let rowColumnNumber = parseInt(Math.sqrt(vignetteNumber));
            // compute col width and row height
            let colWidth = CANVAS_WIDTH / rowColumnNumber;
            let rowHeight = CANVAS_HEIGHT / rowColumnNumber;
            for (let col = 0 ; col < rowColumnNumber ; col++) {
              let xPos = col * colWidth;
              for (let row = 0 ; row < rowColumnNumber ; row++) {
                let yPos = row * rowHeight;
                ctx.drawImage(fullSizeVignette, xPos, yPos, colWidth , rowHeight);
              }
            }
            break;
          }
        } else {
          alert (STRING_ERROR_BROWSER_NOT_SUPPORTED);
          return 0;
        }
        // make canvas appear and resize to screen
        canvas.setAttribute ("style", "display: flex; width: 100%; height: auto; margin: auto; margin-top: max(5px, 5%); margin-bottom: max(5px, 5%);")
    }

    function createVignetteCanvas (qrCode, title, pubkey, infos, givenImage, isLogo, isCard, frameLineSize){
      let canvas = document.createElement('canvas')
      if (canvas.getContext) {
        let ctx = canvas.getContext('2d');
        canvas.setAttribute ("width", CANVAS_WIDTH);
        canvas.setAttribute ("height", CANVAS_HEIGHT);
        let alignTextOn = 2 * MARGIN_CANVAS + IMAGE_WIDTH_AND_HEIGHT;
          
        // title
        ctx.font = TITLE_FONT;
        ctx.textBaseline = "top";
        title = replaceG(title)
        ctx.fillText (title, alignTextOn, MARGIN_CANVAS);

        // infos
        ctx.font = INFOS_FONT;
        maxwidth = CANVAS_WIDTH - alignTextOn - MARGIN_CANVAS
        writeInfosOnCanvas (infos, ctx, alignTextOn, INFOS_START_POSITION, maxwidth, INFOS_LINE_BREAK);

        // qrcode
        imageQrCode = document.getElementById("vignetteQrCode").children[0];
        // center QRcode to image
        xPosQr = parseInt(MARGIN_CANVAS + IMAGE_WIDTH_AND_HEIGHT /2 - QR_WIDTH_AND_HEIGHT/2)
        // center it vertically between image and the pubkey
        yPosQr = parseInt( MARGIN_CANVAS + IMAGE_WIDTH_AND_HEIGHT + ((CANVAS_HEIGHT - (2* MARGIN_CANVAS + IMAGE_WIDTH_AND_HEIGHT)) /2 - QR_WIDTH_AND_HEIGHT/2 ) )
        ctx.drawImage(imageQrCode, xPosQr, yPosQr );

        // pubkey
        if (isCard === false) {
          // write full-length pubkey
          ctx.font = PUBKEY_URI_FONT;
          yPosPubkey = parseInt(CANVAS_HEIGHT - MARGIN_CANVAS);
          ctx.fillText (pubkey, MARGIN_CANVAS, yPosPubkey);
        } else {
          // Write short pubkey centered on QRcode
          ctx.font = INFOS_FONT;
          short_pubkey = pubkey.substring(0,4) + "…" + pubkey.substring(pubkey.length - 8, pubkey.length-4);

      /***** don't show checksum until main client (cesium) displays it
      colon_cksum = pubkey.substring(pubkey.length - 4, pubkey.length);
          // measure text length
      let len_pk_ck = ctx.measureText(short_pubkey + colon_cksum).width;
      // compute start coordinates
      yPosPubkey = parseInt(yPosQr + QR_WIDTH_AND_HEIGHT + MARGIN_CANVAS / 2);
      xPosPubkey = parseInt((xPosQr + QR_WIDTH_AND_HEIGHT/2) - len_pk_ck / 2);
      xPosCksum = xPosPubkey + ctx.measureText(short_pubkey).width; 
      // write short pubkey
          ctx.textAlign = "start";
          ctx.fillText (short_pubkey, xPosPubkey , yPosPubkey);
      // write checksum in grey
      ctx.save();
      ctx.fillStyle = CKSUM_COLOR;
      ctx.font = "italic " + INFOS_FONT;
          ctx.fillText (colon_cksum, xPosCksum , yPosPubkey);
      ctx.restore();*/

      // to be removed once Cesium (or another widely used client) displays ck.
      yPosPubkey = parseInt(yPosQr + QR_WIDTH_AND_HEIGHT + MARGIN_CANVAS / 2);
          xPosPubkey = parseInt(xPosQr + QR_WIDTH_AND_HEIGHT / 2);
          ctx.textAlign = "center";
          ctx.fillText (short_pubkey, xPosPubkey , yPosPubkey);
        }

        // image
        let pos = squareImage (givenImage);
        ctx.drawImage(givenImage, pos[0], pos[1], pos[2], pos[2], MARGIN_CANVAS, MARGIN_CANVAS, IMAGE_WIDTH_AND_HEIGHT, IMAGE_WIDTH_AND_HEIGHT);

        // URI
        if (isCard === false) {
          drawURL(ctx);
        }

        // logo
        if (isLogo === true ){
          logo = document.getElementById("logo_g_png");
          xLogo = parseInt (MARGIN_CANVAS + IMAGE_WIDTH_AND_HEIGHT - LOGO_WIDTH_AND_HEIGHT + MARGIN_CANVAS / 2 )
          yLogo = parseInt (MARGIN_CANVAS + IMAGE_WIDTH_AND_HEIGHT - LOGO_WIDTH_AND_HEIGHT + MARGIN_CANVAS / 2 )
          ctx.drawImage ( logo, xLogo, yLogo, LOGO_WIDTH_AND_HEIGHT, LOGO_WIDTH_AND_HEIGHT);
        }

        // frame
        if (frameLineSize > 0) {
          ctx.lineWidth = frameLineSize;
          ctx.strokeStyle = 'black';
          ctx.strokeRect ( 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        }
        // background
        ctx.globalCompositeOperation = "destination-over";
        ctx.fillStyle = "#FFFFFF";
        ctx.fillRect ( 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

        return canvas;

      } else {
        alert (STRING_ERROR_BROWSER_NOT_SUPPORTED);
        return 0;
      }
    }

    function drawURL(ctx){
      ctx.font = PUBKEY_URI_FONT;
      address = document.baseURI;
      ctx.save();
      ctx.fillStyle = "grey";
      switch(URL_ORIENTATION) {
        case "horizontal":
          xPosURL = CANVAS_WIDTH - MARGIN_CANVAS;
          yPosURL = yPosPubkey;
          ctx.textAlign = "end";
          ctx.fillText (address, xPosURL, yPosURL);
          break;
        case 'vertical':
          let metrics = ctx.measureText(address);
          let addressWidth = metrics.width;
          // write URI on the right side
          ctx.translate(CANVAS_WIDTH - MARGIN_CANVAS, CANVAS_HEIGHT - MARGIN_CANVAS);
          ctx.rotate(-Math.PI/2);
          ctx.fillText (address, 0, 0);
          break;
        default:
          break;
      }
      ctx.restore();
    }

    function writeInfosOnCanvas (infos, ctx, x, y, maxwidth, lineSpace){
      state = ["normal"];
      let infosList = infos.split ("\n");
      for (let info of infosList) {
        info = replaceG(info);
        y = wrapText(ctx, info, x, y, maxwidth, lineSpace);
        y += lineSpace;
      }
    }

    function wrapText(context, text, x, y, maxWidth, lineHeight) {
      let words = text.split(' ');
      let line = '';
      for(let n = 0; n < words.length; n++) {
        let testLine = line + words[n] + ' ';
        let metrics = context.measureText(testLine);
        let testWidth = metrics.width;
          if (testWidth > maxWidth && n > 0) {
            context.fillText(line, x, y);
            line = words[n] + ' ';
            y += lineHeight;
          } else {
          line = testLine;
        }
      }
    context.fillText(line, x, y);
    return y;
    }

    function squareImage (image) {
      // gets an image, returns x, y and dimension of the square to take
      if (image.width === image.height) {
        x = 0 ; y = 0 ; dim = image.width;
      } else if ( image.height > image.width) {
        x = 0;
        y = image.height / 2 - (image.width / 2 );
        dim = image.width;
      } else if ( image.width > image.height) {
        y = 0;
        x = image.width / 2 - (image.height / 2 );
        dim = image.height;
      }
    return [x, y, dim]
    }

    function replaceG (text){
      text = text.replace ( G1_WRONG_SYMBOL, G1_SYMBOL );
      text = text.replace ( G_WARN_SYMBOL, G_SYMBOL );
      text = text.replace ( G1_MIN_WRONG_SYMBOL, G1_MIN_SYMBOL );
      text = text.replace ( G_MIN_WARN_SYMBOL, G_MIN_SYMBOL );
      return text;
    }

    function downloadVignette(){
      let download = document.getElementById("download");
      let image = document.getElementById("vignette_canvas").toDataURL("image/png")
                  .replace("image/png", "image/octet-stream");
      download.setAttribute("href", image);
    }

    function printVignette(numberOfVignettes){
      let printWindow = window.open();
      let image = document.getElementById("vignette_canvas").toDataURL("image/png")
                  .replace("image/png", "image/octet-stream");
      let layout = (numberOfVignettes == 2) ? "portrait" : "landscape";
      printWindow.document.write('<!DOCTYPE html>\
      <html><head>\
        <style type="text/css">\
          @media print {\
            @page {size: A4 ' + layout + '; margin: 0;}\
            img { width: 97%; height: 97%;  }\
          }\
        </style>\
      </head>\
      <body><img src="' + image + '" onload="window.print();window.close()" /></body></html>'
      );
      printWindow.focus();
    }

    function copyPubkey() {
        let copy_message = Function_STRING_SEND_DONATION(CREATOR_PUBKEY_CK);
        try {
          if (navigator.clipboard.writeText(CREATOR_PUBKEY_CK)) {
            copy_message += STRING_PUBKEY_COPIED_IN_CLIPBOARD;
          }
        } catch {
          // Either pubkey could not be copied, or navigator.clipboard is not available... Simple alert message does the job.
        }
        alert(copy_message);
      }
