// From https://github.com/45678/Base58
var ALPHABET, ALPHABET_MAP, Base58, i;

var Base58 = {};

ALPHABET = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

ALPHABET_MAP = {};

i = 0;

while (i < ALPHABET.length) {
  ALPHABET_MAP[ALPHABET.charAt(i)] = i;
  i++;
}

Base58.encode = function(buffer) {
  var carry, digits, j;
  if (buffer.length === 0) {
    return "";
  }
  i = void 0;
  j = void 0;
  digits = [0];
  i = 0;
  while (i < buffer.length) {
    j = 0;
    while (j < digits.length) {
      digits[j] <<= 8;
      j++;
    }
    digits[0] += buffer[i];
    carry = 0;
    j = 0;
    while (j < digits.length) {
      digits[j] += carry;
      carry = (digits[j] / 58) | 0;
      digits[j] %= 58;
      ++j;
    }
    while (carry) {
      digits.push(carry % 58);
      carry = (carry / 58) | 0;
    }
    i++;
  }
  i = 0;
  while (buffer[i] === 0 && i < buffer.length - 1) {
    digits.push(0);
    i++;
  }
  return digits.reverse().map(function(digit) {
    return ALPHABET[digit];
  }).join("");
};

Base58.decode = function(string) {
  var bytes, c, carry, j;
  if (string.length === 0) {
    return "";
  }
  i = void 0;
  j = void 0;
  bytes = [0];
  i = 0;
  while (i < string.length) {
    c = string[i];
    if (!(c in ALPHABET_MAP)) {
      throw "Base58.decode received unacceptable input. Character '" + c + "' is not in the Base58 alphabet.";
    }
    j = 0;
    while (j < bytes.length) {
      bytes[j] *= 58;
      j++;
    }
    bytes[0] += ALPHABET_MAP[c];
    carry = 0;
    j = 0;
    while (j < bytes.length) {
      bytes[j] += carry;
      carry = bytes[j] >> 8;
      bytes[j] &= 0xff;
      ++j;
    }
    while (carry) {
      bytes.push(carry & 0xff);
      carry >>= 8;
    }
    i++;
  }
  i = 0;
  while (string[i] === "1" && i < string.length - 1) {
    bytes.push(0);
    i++;
  }

 return bytes.reverse();
};


// ---- in html (from Tuxmain)----


const FORMAT_ERR = "Invalid format";

function fill_pubkey(pubkey) {
	return "\x00".repeat(32-pubkey.length)+pubkey;
}

// This is a simple oneliner in Python. JS is one of the worst scripting languages ever.
async function gen_checksum(pubkey) {
	// Why does this need async?
	var checksum = await window.crypto.subtle.digest('SHA-256', new Uint8Array(pubkey));
  console.log(checksum);
	// Why on Earth the await needs an assignation and can't be used directly? This is stupid.
	var checksum = await window.crypto.subtle.digest('SHA-256', new Uint8Array(checksum));
  return Base58.encode(new Uint8Array(checksum)).slice(0, 3);
}

// Matograine mix

function check_B58(str) {
	return str.match(/^[1-9A-HJ-NP-Za-km-z]*$/) !== null;
}

//public_key_checksum_from_b58(string_b58) → (string_b58)
//return a checksum of a public key
//return -1 if bad format of public key
async function public_key_checksum_from_b58(public_keyb58) {
	if (check_B58(public_keyb58) == false)
		return -1;
	var public_key = Base58.decode(public_keyb58);
	if (public_key.bytesLength > 32){
		return -1;
  }
  //pubkey = fill_pubkey(pubkey);
	return await gen_checksum(public_key);
};

